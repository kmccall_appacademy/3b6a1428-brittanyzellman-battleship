class Board
  attr_reader :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    @grid = Array.new(10){ Array.new(10) }
  end

  def count
    count = 0
    grid.each do |arr|
      arr.each do |el|
        count += 1 if el == :s
      end
    end
    count
  end

  def won?
    count == 0
  end

  def empty?(pos = nil)
    if pos != nil
      @grid[pos.first][pos.last] == nil ? true : false
    else
      count == 0 ? true : false
    end
  end

  def full?
    count = 0
    @grid.each do |arr|
      arr.each do |el|
        count += 1 if el == nil
      end
    end
    count == 0 ? true : false
  end

  def place_random_ship
    # debugger
    x = @grid.length
    if !full?
      @grid[rand(x)][rand(x)] = :s
    else
      raise "Board is full"
    end
  end

  def [](pos)
    x, y = pos
    @grid[x][y]
  end

  def []=(pos, val)
    x, y = pos
    @grid[x][y] = val
  end

end
