require 'byebug'

class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
  end

  def attack(position)
    # debugger
    if @board.empty?(position)
      @board.grid[position.first][position.last] = :x
    end
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def play_turn
    attack(@player.get_play)
  end

end
